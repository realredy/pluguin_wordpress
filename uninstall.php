<?php

/*
 * @package poch
 * @version 1.7.2
 */



/* Primero nos aseguramos de que la variable WP_UNISTALL_PLUGIN esta definida ya que esta es la encargada de 
eleborar el proceso de desintatalcion del plugin  */

if(!defined( 'WP_UNINSTALL_PLUGIN' )){
    die; 
}
/* como este plugin crea un post type procedemos a eliminar de la base de datos todo lo relacionado a la
creacion de este plugin y con esto limpiamos la base de datos de todo lo relacionado con el plugin
*/
//get_post($post, $output, $filter)
//$post = eligimos que tipo de elemento eliminaremos en este caso es un 'post_type' 
//$ouput = este es el nombre del post que hemos creado en mi caso se llama 'servicios' 
//$filter = en mi caso aplicamos el filtro que llamara a todos los post con en signo de -get_alloptions_110()


// con esto optenemos todosos los post llamado servicios y con menos 1 selecciona todo a partir de -1, 0, 1....
//$service = get_posts(array('post_type'=>'servicios', 'numberposts' => -1)); // return array de post






                            //------------=====   PROCESO WP DELETE POST  ========---------//

$args = array(
    'numberposts' => -1,
    'post_type'   => 'servicios'
  );
   
  $latest_books = get_posts( $args );
 
// foreach barremos la lisa para eliminar uno a uno
foreach ($latest_books as $servic) {
   wp_delete_post($servic->ID, true);
}


                          //------------=====   PROCESO WP DELETE POST  ========---------//







/* Cuando usamos el proceso anterior es posible que queden rastros de la base de datos entonces es mas seguro ir
por todos los rastros que pueden involucrar la creacion de un post. 
--- al declarar la variable global wpdb podemos crear querys y anadir editar o eliminar todo desde la base de datos 

+++ en un post se crean archivos en wp_post - wp-postmeta - wp_term_relationships estos se deberan eliminar +++ */

global $wpdb;   // -- llamamos la variable global !peligrosa

// alt25 para la flecha... emet crea un class con las tres ... al final  
// ↓ -- Eliminamos el post type servicios..  la variable al eliminar guarda datos como id, post_type ect...
$wpdb->query( "DELETE FROM wp_post WHERE post_type = 'servicios' " );
// ↓ Eliminamos los metabox en caso de que existan
$wpdb->query( "DELETE FROM wp_postmeta WHERE post_id NOT IN  (SELECT id FROM wp_post)" );
//  ↓ eliminamos el relation shipt de la base de datos 
$wpdb->query( "DELETE FROM wp_term_relationships WHERE object_id NOT IN (SELECT id FROM wp_post)" );