<?php 


/**
 * @package poch
 * @version 1.7.2
 */
/*
Plugin Name: poch_plg
Plugin URI: http://wordpress.org/plugins/
Description: This is not just a plugin, it symbolizes the hope and enthusiasm of an entire generation summed up in two words sung most famously by Louis Armstrong: Hello, Dolly. When activated you will randomly see a lyric from <cite>Hello, Dolly</cite> in the upper right of your admin screen on every page.
Author: Matt Mullenweg
Version: 1.7.2
Author URI: http://ma.tt/
*/

/**

* Add custom field to the checkout page

*/


/*
Confirmamos esta variable para asegurarnos de que esta variable esta definida la cual cumple el rol 
de decirle al sistema que en esta variable se guarda la ruta hacia los directorios del wordpress
*/
 if(!defined('ABSPATH')){
 	die;
 }
 /*
Nos aseuramos de eque esta funcion esta activa para evitar vulnerabilidades en la base de datos y el s
sistema ya que con esta funcion es como se activan las diferentes funciones que ejemplo crean posType ect...
 */
if(! function_exists('add_action')){
	echo 'esta funcion debe estar definida para la ejecucion de todo el sistema';
	die; // die para que pare toda la ejecucion de las de las dependencias 
}

if(file_exists(plugin_dir_path(__FILE__).'includ\class_pl_links.php')){
   include_once plugin_dir_path(__FILE__).'includ\class_pl_links.php';
}else{
   echo 'no encuentro este file: '. plugin_dir_path(__FILE__).'includ\class_pl_links.php';
}

class realPlugin {

function __construct(){
   /*
   Con este add_action indicamos que inscriba este custom post al sistema... nota que 
   auun no se ha llamado la funcion y esta será llamada al activar el plugin
   */
   add_action( 'init', array($this, 'add_property_post') );
   
   /*
   *Agregamos la pagina de administracion con este menu de administracion 'admin_menu' y con el array 
   le decimos que $this = usa esta clase luego llama a esta funcion 'pagina_deAdministracion'
   */
   add_action( 'admin_menu', array($this, 'pagina_deAdministracion'));


   
   add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), array( 'realPlugin' , 'add_action_links' ) );

}
        
  static function add_action_links($links){ 
   
   return pluginLinks::n_links($links);
   //    $linksums = '<a href="admin.php?page=poch_plg">setings</a>';
   //    $linksum = '<a href="http://lavour.es" target="_blank">author</a>';
   //   array_push($links,$linksum);
   //   array_push($links,$linksums);
   //    return $links; 
  }


   public function pagina_deAdministracion(){

     add_menu_page( 'poch plg', 'realMenu', 'manage_options', 'poch_plg', array($this, 'get_pageTheme'), 'dashicons-image-filter', 120 );
   }

   function get_pageTheme(){
      // plugin_dir_path(__FILE__) ↓
      // C:\xampp\htdocs\practice\wp-content\plugins\pluguin_wordpress/
      require_once  plugin_dir_path(__FILE__). 'includ/theme.php';
   }


       function registrador_de_estilos(){
  //para colocar este estilo en el front debemos cambiar admin_enqueue_scripts -> wp_enqueue_scripts
          add_action('admin_enqueue_scripts', array($this, 'colocarEstilos_scripties')); 
       }

/*
con este contructor estariamos imprimiendo el hola mundo ya que __construct(); funcionaria como una 
funcion, si el parametro que hemos pasado fuese un array entonces antes del parametris deberiamos 
colocar __contruct(array $parameter)
  */ 

     /*
	 * Estos siguientes metodos o funciones seran llamados por wordpress una vez detecte una de estas 
	  funciones las cuales no seran llamadas desde la clase ni arrojaran errores ya que estas se eje-
	  cutan durante estos tres estado del plugin
	 */
  function activate(){
     // en este punto que es en donde se crearan todas las acciones del pluguin podemos decir que ejemp. podemos crear un cpt
     //flus rewrite rules: nos dice que check todo lo que esta en este nivel y lo reescriba ya que el pl se ha activado

     $this->add_property_post();
     flush_rewrite_rules(); 
   
  }
  function deactivate(){
     // flush rewrite rules: en este punto no debemos ejemp. eliminar el cpt ya que al desactivarlo wp entiende que todas las 
     // creadas anteriormente se eliminan producto de la desactivacion, entonces lo que debemos hacer es reescribir las reglas 
     // para que wp refresque la memoria dejando todo limplio para que al activarse las funciones entren sin conflictos

  }
 //-------  function uninstall(){
   /* En este punto se debe considerar que el usuario no desea mas el plugin y entonces cuando debemos decidir si eliminamos 
   todos los rastros dejado por el pl. me refiero a la base de datos y los archivos que pudiera generar el plugin*/
 //-----  }


  function add_property_post(){
     //$arg funcionalidades del post
     $args = array('public'      => true,
                    'description'=>'mi firsr custom post', 
                     'labels'    => array('name'           => 'realPosts',
                                           'singular_name' => 'realPost',
                                          'menu_name'      => 'realPost'), 
                                           //puedes agregar mas items aca son muchis
                     'public'        => true,   
                     'show_in_menu'  => true, 
				      'menu_icon'              => 'dashicons-plus-alt',
                     'menu_position' => 5,
                     'show_in_admin_bar'     => true,
                     'show_in_nav_menus'     => true,
                     'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
                              );
                                           
      register_post_type( 'Servicios', $args );
       
  }

                                  // ↓ Colocando los css & js a la pagina ///

         function colocarEstilos_scripties(){
            // Agregamos el estilo para el backend
          wp_enqueue_style('back_style', plugins_url("/recursos/pc_estilos.css", __FILE__));
             // Agregamos el script para el backend
          wp_enqueue_script('back_script', plugins_url("/recursos/pc_scripts.js", __FILE__));
         }
}

// es recomendable crear un if para asegurarnos de que la clase que hemos creado existe
 if(class_exists('realPlugin')){
        $cltriguer = new realPlugin();
//	 $cltriguer = new realPlugin( 'hola mundo' );	//--si ubiesemos contruido esta classe para usarla como 
// una funcion, deberiamos crear un constructor dentro de la clasae para que este se encargue de ejecutar 
//recibir estos datos que estasmos pasando por paramentros 
      $cltriguer->registrador_de_estilos();
  
 }
 
/* Wordpress tiene tres funciones que se ejecutan dutante varios momentos de ejecucion del plugin
// activation : se ejecuta cuando el plugin se ha activado
// llamamos al hook de activacion del plugin para que este este funcional y pueda ejecutarse y al estar
en una clase debemos emplear un array indicando el nombre de la classe que contine el metodo y luego el 
nombre del método*/
register_activation_hook(__fILE__ , array($cltriguer, 'activate'));
// deactivation: se ejecuta cuando el plugin ha siso desactivado
register_deactivation_hook(__fILE__ , array($cltriguer, 'deactivate'));

//uninstall: se ejecuta cuando el pluguin se ha desInstalado
// (____register_uninstall_hook(__fILE__ , array($cltriguer, 'uninstall'));
/*
                                   UNINSTALL EXPLICACION

Cuando un susuario intenta ejecutar este hook al mismo tiempo es como su estuviera leyendo los hooks que 
instalan el plugin causando un conflicto... entonces wp avilita la lectura de un archivo llamado 
uninstall.php y este es llamado para ejecutar todo lo necesario durante la desintalacion.
*/







